const xevEmitter = require("xev-emitter")(process.stdin);
const SerialPort = require("serialport");

const specialKeys = {
  BackSpace: 8,
  Shift_L: 15,
  Shift_R: 15,
  space: 32,
  comma: 44,
  question: 63,
  semicolon: 59,
  period: 46,
  exclam: 33,
  colon: 58,
  slash: 47,
  ccedilla: 135,
  eacute: 130,
  egrave: 138,
  agrave: 133,
  parenleft: 40,
  parenright: 41,
  apostrophe: 39,
  minus: 45,
  equal: 61,
  Caps_Lock: 14, // we use shift out as there is no capslock ascii code
  Return: 10,
};

const shiftOut = 14;

const port = new SerialPort("/dev/ttyUSB0", {
  baudRate: 9600,
});

port.on('data', (data) => {
  console.log('Data:', data.toString())
})

xevEmitter.on("KeyPress", (key) => {
  console.log(key.charCodeAt(0), "was pressed");
  if (specialKeys[key]) {
    port.write(String.fromCharCode(specialKeys[key]));
  } else {
    port.write(key);
  }
});

xevEmitter.on("KeyRelease", (key) => {
  console.log(key, "was released");
  // temp use this for shift
  if (key === "Shift_L" || key === "Shift_R") {
    port.write(String.fromCharCode(shiftOut));
  }
});
