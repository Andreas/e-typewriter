#include "Inkplate.h"
#include "SdFat.h"

#include "Fonts/Open_Sans_SemiBold_32.h"

extern Inkplate display;

const GFXfont *text_font = &Open_Sans_SemiBold_32;

enum Ascii
{
    backspace = 8,
    lf = 10,
    shift_out = 14,
    shift_in = 15,
    a = 97,
    z = 122
};

const int ascii_upper_shift = 32;

enum Color
{
    white,
    black
};

const unsigned long max_status_info_time = 5000;
const unsigned long max_inactivity_time = 10000;

const int partial_update_time = 270;

const uint16_t max_line_number = 4096; // arbitrary, can be changed
const int max_file_name_length = 256;
const int max_line_length = 1024; // seems reasonable to think there won't be any line greater than this number ?

const char *cache_file_name = ".cache";

struct EditorState
{
    bool shift_down = false;
    bool need_refresh = false;
    bool need_full_refresh = false;
    bool is_document_dirty = false;
    bool status_info_displayed = false;

    String str_to_print;
    String full_text;
    String current_file_name;
    // @todo maybe store it in uint32_t ?
    uint16_t screen_line_indexes[max_line_number]; // store the text char index oof each line on screen (can be either a word wrapping or \n)
    uint16_t current_line_index = 0;

    unsigned long previous_loop_time;
    unsigned long inactivity_time;
    unsigned long current_status_info_time;
};

static struct EditorState state;

bool will_cursor_x_be_out(int16_t to_remove, int16_t to_add)
{
    return display.getCursorX() - to_remove < 0 || display.getCursorX() + to_add > display.width();
}

bool will_cursor_y_be_out(int16_t to_remove, int16_t to_add)
{
    return display.getCursorY() - to_remove < 0 || display.getCursorY() + to_add > display.height();
}

uint16_t get_text_width(String text)
{
    int16_t x, y;
    uint16_t w, h;

    display.getTextBounds(String(text), 0, 0, &x, &y, &w, &h);
    return w;
}

uint16_t get_text_height(String text)
{
    int16_t x, y;
    uint16_t w, h;

    display.getTextBounds(String(text), 0, 0, &x, &y, &w, &h);
    return h;
}

void erase_last_string(String last_str)
{
    int16_t cursor_x = display.getCursorX();
    int16_t cursor_y = display.getCursorY();
    int16_t x, y;
    uint16_t w, h;

    display.getTextBounds(last_str, cursor_x, cursor_y, &x, &y, &w, &h);
    uint16_t real_width = w;
    display.fillRect(cursor_x - real_width, y, real_width, h, white);
    // for debug purpose
    // display.drawRect(cursor_x - real_width, y, real_width, h, black);

    display.setCursor(cursor_x - real_width, cursor_y);
}

String get_line(uint16_t line_index)
{
    if (line_index == state.current_line_index)
    {
        return state.full_text.substring(state.screen_line_indexes[line_index]);
    }
    else
    {
        return state.full_text.substring(state.screen_line_indexes[line_index], state.screen_line_indexes[line_index + 1]);
    }
}

String get_current_line()
{
    return get_line(state.current_line_index);
}

void redraw_screen_from_line(uint16_t line_index)
{
    uint16_t index = line_index >= 0 ? line_index : 0;

    display.clearDisplay();
    // @todo : calculate height based on text height
    display.setCursor(0, 10);

    while (index < state.current_line_index)
    {
        // @todo : display all remaining lines till end of the screen reach or end of the text
        String line_to_display = get_line(index);
        line_to_display.replace("\n", "");
        display.println(line_to_display);
        index++;
    }
}

void handle_new_page()
{
    uint16_t line_height = get_text_height("A");

    if (will_cursor_y_be_out(0, line_height * 3))
    {
        redraw_screen_from_line(state.current_line_index - 2);
        state.need_full_refresh = true;
    }
}

void new_screen_line(uint16_t start_line_index)
{
    state.current_line_index++;
    state.screen_line_indexes[state.current_line_index] = start_line_index;
}

void remove_last_screen_line()
{
    state.screen_line_indexes[state.current_line_index] = 0;
    state.current_line_index--;
}

void remove_last_char()
{
    char last_char = state.full_text.charAt(state.full_text.length() - 1);
    if (last_char)
    {
        if (state.str_to_print.length() > 0)
        {
            // No need to update display since the char hasn't been displayed yet
            state.str_to_print.remove(state.str_to_print.length() - 1);
        }
        else
        {
            // If it was the first line char, then move the cursor to previous line's end.
            if (state.current_line_index != 0 && state.full_text.length() == state.screen_line_indexes[state.current_line_index])
            {
                remove_last_screen_line();
                String current_line = get_current_line();
                int16_t x, y;
                uint16_t line_width, line_height;
                int16_t cursor_x = display.getCursorX();
                int16_t cursor_y = display.getCursorY();

                display.getTextBounds(current_line, 0, 0, &x, &y, &line_width, &line_height);
                display.setCursor(line_width, cursor_y - line_height);
            }
            // @todo remove the \n if it is the last line char ?
            erase_last_string(String(last_char));
        }
        state.full_text.remove(state.full_text.length() - 1);
        state.need_refresh = true;
    }
}

void draw_status_bar()
{
    int16_t line_y = display.height() - get_text_height("A");
    display.drawLine(0, line_y, display.width(), line_y, black);
}

void clear_status_bar()
{
    display.fillRect(0, display.height() - get_text_height("A") + 1, display.width(), get_text_height("A"), white);
    state.status_info_displayed = false;
    state.need_refresh = true;
}

void status_info(String info)
{
    int16_t cursor_x = display.getCursorX();
    int16_t cursor_y = display.getCursorY();

    display.setCursor(0, display.height() - get_text_height("A"));
    display.print(info);
    display.setCursor(cursor_x, cursor_y);
    state.current_status_info_time = 0;
    state.status_info_displayed = true;
    state.need_refresh = true;
}

void handle_text_wrapping()
{
    uint16_t to_print_width = get_text_width(state.str_to_print);
    if (will_cursor_x_be_out(0, to_print_width))
    {
        // look for a space in the current line
        String current_line = get_current_line();
        int space_index = current_line.lastIndexOf(" ");
        if (space_index != -1 && space_index < current_line.length() - 1)
        {
            // then move it to the next line
            String word_to_wrap = current_line.substring(space_index + 1);
            erase_last_string(word_to_wrap);
            display.println();
            new_screen_line(state.screen_line_indexes[state.current_line_index] + space_index + 1);
            handle_new_page();
            // @bug don't know why last char is display twice without this ???
            display.print(word_to_wrap.substring(0, word_to_wrap.length() - 1));
        }
        else
        {
            // otherwise cut the current word
            display.println();
            new_screen_line(state.full_text.length() - 1);
            handle_new_page();
        }
        state.need_refresh = true;
    }
}

// Inspire from https://developer.wordpress.org/reference/functions/sanitize_file_name/
// Maybe look at https://github.com/WordPress/wordpress-develop/blob/5.8.1/src/wp-includes/formatting.php#L1585 for accent ?
String sanitize_file_name(String name)
{
    String file_name = name;
    String to_replace_str[] = {"à", "â", "è", "é", "ê", "ô", "ç", "ù", ".", " ", "'"};
    String replacement[] = {"a", "a", "e", "e", "e", "o", "c", "u", "_", "_", "_"};
    String to_remove[] = {"?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", "%", "+", "’", "«", "»", "”", "“", "\n"};

    file_name.toLowerCase();

    for (int i = 0; i < sizeof(to_remove) / sizeof(to_remove[0]); i++)
    {
        file_name.replace(to_remove[i], String(""));
    }

    file_name.trim();
    // @todo remove multiple space occurence

    // sizeof(to_replace_str) / sizeof(to_replace_str[0])
    for (int i = 0; i < 10; i++)
    {
        file_name.replace(to_replace_str[i], replacement[i]);
    }

    return file_name;
}

//@todo use new RTC API to add timestamp to filenames
String get_next_file_id(String dir_path)
{
    SdFile root;
    SdFile file;
    uint16_t new_id = 0;
    char file_name[max_file_name_length];

    // @todo : handle multiple folders and maybe multiple users
    if (!root.open(dir_path.c_str()))
    {
        display.print("open root failed");
    }

    while (file.openNext(&root, O_RDONLY))
    {
        file.getName(file_name, sizeof(file_name));
        file.close();

        long num = String(file_name).substring(0, 3).toInt();
        if (num != 0 && num > new_id)
        {
            new_id = num;
        }
    }
    new_id++;
    char str_id[4];
    sprintf(str_id, "%03u", new_id);
    return String(str_id);
}

String generate_file_name()
{
    String first_line = get_line(0);
    String file_name = sanitize_file_name(first_line);
    file_name = get_next_file_id("/") + "_" + file_name + ".md";
    return file_name;
}

String cut_new_line(String line)
{
    uint16_t index = 1;
    String line_part = line.substring(0, index);

    while (get_text_width(line_part) < display.width() && index < line.length() - 1)
    {
        index++;
        line_part = line.substring(0, index);
    }

    int space_index = line_part.lastIndexOf(" ");
    if (space_index != -1 && space_index < line_part.length() - 1)
    {
        line_part = line_part.substring(0, space_index - 1);
        index = space_index;
    }

    state.full_text += line_part;
    new_screen_line(state.full_text.length() - 1);
    return line.substring(index);
}

void insert_new_line(String line)
{
    uint16_t text_width = get_text_width(line);

    if (text_width < display.width())
    {
        state.full_text += line;
        new_screen_line(state.full_text.length() - 1);
    }
    else
    {
        String line_part = cut_new_line(line);
        insert_new_line(line_part);
    }
}

void load_file(String file_name)
{
    SdFile file;
    char line[max_line_length];

    bool ok = file.open(file_name.c_str(), O_READ);
    if (!ok)
    {
        status_info("Error while opening " + file_name);
        return;
    }
    while (file.available())
    {
        int n = file.fgets(line, sizeof(line));
        if (n <= 0)
        {
            status_info("Error reading file : fgets failed");
        }
        else if (line[n - 1] != '\n' && n == (sizeof(line) - 1))
        {
            status_info("Error reading file : line too long");
        }
        else
        {
            insert_new_line(String(line));
        }
    }
    file.close();

    uint16_t load_line_index = state.current_line_index > 5 ? state.current_line_index - 5 : 0;
    redraw_screen_from_line(load_line_index);
    state.need_full_refresh = true;
}

void load_file_in_cache()
{
    SdFile file;
    char line[max_line_length];

    bool ok = file.open(cache_file_name, O_READ);
    if (!ok)
    {
        //@todo maybe don't treat it like an error, cache file may be absent.
        status_info("Erreur while opening cache file");
        return;
    }

    int n = file.fgets(line, sizeof(line));
    file.close();
    if (n <= 0)
    {
        status_info("Error reading file : fgets failed");
    }
    else
    {
        load_file(String(line));
    }
}

void save_cache_file()
{
    SdFile file;

    bool ok = file.open(cache_file_name, O_RDWR | O_CREAT | O_TRUNC);
    if (ok)
    {
        file.print(state.current_file_name);
        file.close();
    }
}

void save_text()
{
    // We save only if there is more than one line
    // It's a convention, the first line is the filename
    if (state.current_line_index <= 0)
    {
        return;
    }

    SdFile file;
    bool save_cache = false;

    if (state.current_file_name == "")
    {
        state.current_file_name = generate_file_name();
        save_cache = true;
    }

    bool ok = file.open(state.current_file_name.c_str(), O_RDWR | O_CREAT | O_TRUNC);
    if (!ok)
    {
        status_info("Error opening " + state.current_file_name + " !");
        return;
    }
    file.println(state.full_text);
    file.close();
    state.is_document_dirty = false;
    status_info("Document saved.");

    if (save_cache)
    {
        save_cache_file();
    }
}

void editor_on_key_released(char c)
{
    state.inactivity_time = 0;
    // We simulate shift pressed/released with ascci shift in/out char
    if (c == shift_out)
    {
        state.shift_down = false;
    }
}

void editor_on_key_pressed(char c)
{
    state.inactivity_time = 0;
    state.is_document_dirty = true;
    // @todo handle pressed and released for shift
    // @todo maybe handle also key at key pressed instead of released
    if (c == shift_in)
    {
        state.shift_down = true;
    }
    else if (c == backspace)
    {
        remove_last_char();
    }
    else
    {
        if (state.shift_down && (c >= a && c <= z))
        {
            c = c - ascii_upper_shift;
        }
        state.full_text += c;
        state.str_to_print += c;
        if (c == lf)
        {
            new_screen_line(state.full_text.length() - 1);
        }
        else
        {
            handle_text_wrapping();
        }
        handle_new_page();
    }
}

void test_sanitize_file_name()
{
    String test("à");
    display.println("Taille de a : " + String(test.length()));
    String name = sanitize_file_name("  Bonjour jé sùis à fônd !/:;,?%*$&#  ");
    if (name == "bonjour_je_suis_a_fond")
    {
        display.println("sanitize_file_name OK");
    }
    else
    {
        display.println("sanitize_file_name KO");
        display.println(name);
    }
}

void print_text_bounding_box(String text)
{
    // debug
    int16_t cursor_x = display.getCursorX();
    int16_t cursor_y = display.getCursorY();
    int16_t x, y;
    uint16_t w, h;
    display.getTextBounds(String(text.charAt(text.length() - 1)), cursor_x, cursor_y, &x, &y, &w, &h);
    display.drawRect(cursor_x - w, y, w, h, black);
}

void editor_setup()
{
    display.begin();        // Init library (you should call this function ONLY ONCE)
    display.clearDisplay(); // Clear any data that may have been in (software) frame buffer.

    display.setTextSize(3);
    display.setTextWrap(false);
    // @temp : there seems to be a bug in Adafruit_GFX::charBounds (which is used by getTextBounds)
    // Which make it almost unusable...
    // display.setFont(text_font);

    display.setCursor(0, 300);
    draw_status_bar();
    status_info("Ready.");
    // test_sanitize_file_name();
    display.display(); // Clear everything that has previously been on a screen

    state.previous_loop_time = millis();
    state.screen_line_indexes[state.current_line_index] = 0;

    load_file_in_cache();
}

void editor_loop()
{
    unsigned long delta_t = millis() - state.previous_loop_time;

    state.inactivity_time += delta_t;
    state.current_status_info_time += delta_t;

    if (state.inactivity_time >= max_inactivity_time && state.is_document_dirty)
    {
        save_text();
    }

    if (state.current_status_info_time >= max_status_info_time && state.status_info_displayed)
    {
        clear_status_bar();
    }

    if (delta_t > partial_update_time)
    {
        if (state.str_to_print != "")
        {
            display.print(state.str_to_print);
            state.need_refresh = true;
        }

        if (state.need_full_refresh)
        {
            display.display();
            draw_status_bar();
            state.need_full_refresh = false;
        }
        else if (state.need_refresh)
        {
            display.partialUpdate(false, true);
        }
        state.previous_loop_time = millis();
        state.str_to_print = "";
        state.need_refresh = false;
    }
}