#include "Inkplate.h"

#include "Editor.h"

Inkplate display(INKPLATE_1BIT);

bool sd_card_ok = false;

void setup()
{
  Serial.begin(9600);
  if (display.sdCardInit())
  {
    sd_card_ok = true;
    editor_setup();
  }
  else
  {
    display.println("ERROR WITH SD CARD !");
  }
}

void loop()
{
  if (Serial.available())
  {
    char c = Serial.read();
    // @temp simulate key press and release just for shift
    // @todo send also a boolean for pressed or released
    if (c == shift_out)
    {
      editor_on_key_released(c);
    }
    else
    {
      editor_on_key_pressed(c);
    }
  }

  if (sd_card_ok)
  {
    editor_loop();
  }

  delay(50);
}