# e-typewriter

An attempt to build a [Ultimate writer](https://github.com/NinjaTrappeur/ultimate-writer) replication using [Inkplate 6](https://e-radionica.com/en/inkplate-6.html) screen. Similar to [picowriter](https://github.com/deanhuff/picowriter)

# Usage

Upload the project to Inkplate 6

And then run :
`xev | node serial_keyboard.js`

To start sending keyboard data through serial.
